﻿
#include <iostream>
#include <string>
#include <cmath>


using namespace std;



class Animal
{
public:
	virtual void voice()
	{
		cout << "I class animals";
	}
};

class Dog : public Animal
{
public:
	void voice() override
	{
		cout << "woof\t";
	}
};

class Cat : public Animal
{
public:
	void voice() override
	{
		cout << "Miu\t";
	}
};

class Human : public Animal
{
public:
	void voice() override
	{
		cout << "human";
	}
};
int main()
{
	Animal* p[] = { new Dog, new Cat, new Human };
	for (auto i : p)
	{
		i->voice();
	}
}


